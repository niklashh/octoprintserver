#!/bin/bash
[ -z "$2" ] && { echo "usage: $0 findquery devpath."; exit 1; }

mapfile -t devices < <( find /dev -name "$1" )

for device in "${devices[@]}"
do
  if udevadm info -a -n $device |grep -q "ATTRS{devpath}==\"$2\""; then
    echo $device
    exit 0
  fi
done
exit 1
